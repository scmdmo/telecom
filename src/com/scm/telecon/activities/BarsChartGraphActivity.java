package com.scm.telecon.activities;

import java.util.ArrayList;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.scm.telecon.R;
import com.scm.telecon.services.BarsChartFactoryService;
import com.scm.telecon.services.ReadXLS;

public class BarsChartGraphActivity extends Activity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barschartgraph_main);        
        	
        // Getting reference to the button btn_chart
        Button btnChart = (Button) findViewById(R.id.btn_chart);
        Button btn2Chart = (Button) findViewById(R.id.btn2_chart);
        Button btn3Chart = (Button) findViewById(R.id.btn3_chart);
 
        // Defining click event listener for the button btn_chart
        OnClickListener clickListener = new OnClickListener() {
 
            @Override
            public void onClick(View v) {
                // Draw the Income vs Expense Chart
               switch (v.getId()) {
				case R.id.btn_chart:
					totalCosumoElectricida();
					break;
				case R.id.btn2_chart:
					totalCosumoAgua();
					break;
				case R.id.btn3_chart:
					totalCosumoPrecio();
					break;
				default:
					break;
				}
            }
        };
 
        // Setting event click listener for the button btn_chart of the MainActivity layout
        btnChart.setOnClickListener(clickListener);
        btn2Chart.setOnClickListener(clickListener);
        btn3Chart.setOnClickListener(clickListener);
    }
 
    private void totalCosumoElectricida(){
    	BarsChartFactoryService chartFactory = new BarsChartFactoryService(this.getApplicationContext(),
    			getString(R.string.str_btn_chart),
    			"D�as contabilizados",
    			"Valores consumidos");
    	List<String> nameSeries = new ArrayList<String>();
    	nameSeries.add("Watios/d�a");
    	
    	List<Float> consumos = new ArrayList<Float>();
    	List<String> xLabels = new ArrayList<String>();
    	
    	int length=ReadXLS.getCountDias(this.getApplicationContext());    	
    	for(int i=0;i<length;i++){
    		com.scm.telecon.services.Electrodomestic ele=ReadXLS.getConsumoTotalByElectrodomesticosByDia(i, this.getBaseContext());
    		consumos.add(ele.consumo);
    		xLabels.add("D�a "+i);
    	}
    		List<List<Float>> series = new ArrayList<List<Float>>();
    		series.add(consumos);    	
    		
		chartFactory.setxLabels(xLabels);
    	Intent intent=chartFactory.openChart(series,nameSeries);
    	startActivity(intent);
    }
    
    private void totalCosumoAgua(){
    	BarsChartFactoryService chartFactory = new BarsChartFactoryService(this.getApplicationContext(),
    			getString(R.string.str_btn2_chart),
    			"D�as contabilizados",
    			"Valores consumidos");
    	List<String> nameSeries = new ArrayList<String>();
    	nameSeries.add("Litros/d�a");
    	
    	List<Float> consumos = new ArrayList<Float>();
    	List<String> xLabels = new ArrayList<String>();
    	
    	int length=ReadXLS.getCountDias(this.getApplicationContext());
    	for(int i=0;i<length;i++){
    		com.scm.telecon.services.Electrodomestic ele=ReadXLS.getConsumoTotalByAguaByDia(i, this.getBaseContext());
    		consumos.add(ele.consumo);
    		xLabels.add("D�a "+i);
    	}
    		List<List<Float>> series = new ArrayList<List<Float>>();
    		series.add(consumos);    	
    		
		chartFactory.setxLabels(xLabels);
    	Intent intent=chartFactory.openChart(series,nameSeries);
    	startActivity(intent);
    }
    
    private void totalCosumoPrecio(){
    	//valor monetario de la electricidad y el agua (grafica de dos barras)
    	BarsChartFactoryService chartFactory = new BarsChartFactoryService(this.getApplicationContext(),
    			getString(R.string.str_btn2_chart),
    			"D�as contabilizados",
    			"Valor Monetario");
    	List<String> nameSeries = new ArrayList<String>();
    	nameSeries.add("Precio (�) Agua");
    	nameSeries.add("Precio (�) Electricidad");
    	
    	List<Float> consumosAgua = new ArrayList<Float>();
    	List<Float> consumosElec = new ArrayList<Float>();
    	List<String> xLabels = new ArrayList<String>();
    	
    	int length=ReadXLS.getCountDias(this.getApplicationContext());
    	for(int i=0;i<length;i++){
    		com.scm.telecon.services.Electrodomestic agua=ReadXLS.getConsumoTotalByAguaByDia(i, this.getBaseContext());
    		com.scm.telecon.services.Electrodomestic ele=ReadXLS.getConsumoTotalByElectrodomesticosByDia(i, this.getBaseContext());
    		consumosAgua.add(agua.precio);
    		consumosElec.add(ele.precio);
    		xLabels.add("D�a "+i);
    	}
    		List<List<Float>> series = new ArrayList<List<Float>>();
    		series.add(consumosAgua);
    		series.add(consumosElec);
    		
		chartFactory.setxLabels(xLabels);
    	Intent intent=chartFactory.openChart(series,nameSeries);
    	startActivity(intent);
    }
    
    
    @Override
    protected void onStart() {    
    	super.onStart();    	
    }    
    
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}


/**
 * OLD VERSION
*/
//public class BarsChartGraphActivity extends Activity {
//	 
//	private String chartTitle ="Default Title";	
//	private String xTitle ="Default x ais Title";
//	private String yTitle ="Default y axis Title";
//	
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.barschartgraph_main);
// 
//        // Getting reference to the button btn_chart
//        Button btnChart = (Button) findViewById(R.id.btn_chart);
// 
//        // Defining click event listener for the button btn_chart
//        OnClickListener clickListener = new OnClickListener() {
// 
//            @Override
//            public void onClick(View v) {
//                // Draw the Income vs Expense Chart
//                openChart();
//            }
//        };
// 
//        // Setting event click listener for the button btn_chart of the MainActivity layout
//        btnChart.setOnClickListener(clickListener); 
//    }
// 
//    @Override
//    protected void onStart() {    
//    	super.onStart();
//    	chartTitle = getString(R.string.str_btn_chart);
//    	xTitle = "D�as contabilizados";
//    	yTitle = "Valores consumidos";
//    }
//    
//    private void openChart(){
//    	
//    	List<Float> consumos = new ArrayList<Float>(); 
//		List<Float> precios = new ArrayList<Float>(); 
//    	
//    	int length=ReadXLS.getCountDias(this.getApplicationContext());
//    	
//    	for(int i=0;i<length;i++){
//    		com.scm.telecon.services.Electrodomestic ele=ReadXLS.getConsumoTotalByElectrodomesticosByDia(i, this.getBaseContext());
//    		consumos.add(ele.consumo);
//    		precios.add(ele.precio);
//    	}       
//        
//        // Creating an  XYSeries for FirstBar
//        XYSeries firstBarSeries = new XYSeries("Watios/D�a");
//        // Creating an  XYSeries for SecondBar
//        XYSeries secondBarSeries = new XYSeries("Litros/D�a");
//        for(int i=0;i<length;i++){
//        	firstBarSeries.add(i,consumos.get(i).floatValue());
//        	secondBarSeries.add(i,precios.get(i).floatValue());
//        }
//        
//        // Creating a dataset to hold each series
//        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
//        // Adding Income Series to the dataset
//        dataset.addSeries(firstBarSeries);
//        // Adding Expense Series to dataset
//        dataset.addSeries(secondBarSeries);
// 
//        // Creating XYSeriesRenderer to customize incomeSeries
//        XYSeriesRenderer firstBarRenderer = new XYSeriesRenderer();
//        firstBarRenderer.setColor(Color.rgb(130, 130, 230));
//        firstBarRenderer.setFillPoints(true);
//        firstBarRenderer.setLineWidth(2);
//        firstBarRenderer.setDisplayChartValues(true);
// 
//        // Creating XYSeriesRenderer to customize expenseSeries
//        XYSeriesRenderer secondBarRenderer = new XYSeriesRenderer();
//        secondBarRenderer.setColor(Color.rgb(220, 80, 80));
//        secondBarRenderer.setFillPoints(true);
//        secondBarRenderer.setLineWidth(2);
//        secondBarRenderer.setDisplayChartValues(true);
// 
//        // Creating a XYMultipleSeriesRenderer to customize the whole chart
//        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
//        multiRenderer.setXLabels(0);
//        multiRenderer.setChartTitle(this.yTitle);
//        multiRenderer.setXTitle(this.xTitle);
//        multiRenderer.setYTitle(this.yTitle);
//        multiRenderer.setZoomButtonsVisible(true);
//        for(int i=0; i< length;i++){
//            multiRenderer.addXTextLabel(i, "D�a "+i);
//        }
// 
//        // Adding incomeRenderer and expenseRenderer to multipleRenderer
//        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
//        // should be same
//        multiRenderer.addSeriesRenderer(firstBarRenderer);
//        multiRenderer.addSeriesRenderer(secondBarRenderer);
// 
//        // Creating an intent to plot bar chart using dataset and multipleRenderer
//        Intent intent = ChartFactory.getBarChartIntent(getBaseContext(), dataset, multiRenderer, Type.DEFAULT);
// 
//        // Start Activity
//        startActivity(intent); 
//    }	
//	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
//}





