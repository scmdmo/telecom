package com.scm.telecon.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.scm.telecon.R;

public class NuevoElectrodomesticoActivity extends Activity {
	
	static final int CHOOSE_ELECTRO_REQUEST = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);			
		setContentView(R.layout.nuevo_electrodomestico_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void launchEligeElectrodomestico(View view){
		Intent eligeElectrodomestico = new Intent(this,EligeElectrodomesticoActivity.class);
        startActivityForResult(eligeElectrodomestico, CHOOSE_ELECTRO_REQUEST);    	
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case CHOOSE_ELECTRO_REQUEST:
			if (resultCode==RESULT_OK){
				int image_id=data.getIntExtra(EligeElectrodomesticoActivity.EXTRA_DATA,-1);
				if (image_id!=-1){
					ImageButton button = (ImageButton) findViewById(R.id.imageButton3);
					button.setImageResource(image_id);
				}
			}
			break;

		default:
			break;
		}
	}
}
