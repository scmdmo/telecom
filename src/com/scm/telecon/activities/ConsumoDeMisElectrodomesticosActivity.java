package com.scm.telecon.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.scm.telecon.R;
import com.scm.telecon.services.ReadXLS;
import com.scm.telecon.services.Electrodomestic;

public class ConsumoDeMisElectrodomesticosActivity extends Activity {

	int day= 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);			
		setContentView(R.layout.consumo_de_mis_electrodomesticos_main);
		day=0;
		Electrodomestic e = ReadXLS.getConsumoTotalByElectrodomesticosByDia(0, this);
		((TextView) findViewById(R.id.pcTotalElect)).setText((String.valueOf(e.precio).concat(" �")));
		((TextView)findViewById(R.id.cTotalElect)).setText((String.valueOf(e.consumo).concat(" W")));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void lanzarGraficasConsumos(View view) {
	    Intent graficas_consumos = new Intent(this,BarsChartGraphActivity.class);
	    startActivity(graficas_consumos);    	
	 }
	
	public void lanzarVerConsumoDia(View view) {
		day+=1;
		if (day==ReadXLS.getCountDias(getBaseContext()))
			day=0;
		Electrodomestic e = ReadXLS.getConsumoTotalByElectrodomesticosByDia(day, this);
		((TextView) findViewById(R.id.ConsumoDia1)).setText("Consumo d�a "+(day+1));
		((TextView) findViewById(R.id.pcTotalElect)).setText((String.valueOf(e.precio).concat(" �")));
		((TextView)findViewById(R.id.cTotalElect)).setText((String.valueOf(e.consumo).concat(" W")));
	 }
	
	public void lanzarAnteriorConsumoDia(View view) {
		day-=1;
		if (day==-1)
			day=0;
		Electrodomestic e = ReadXLS.getConsumoTotalByElectrodomesticosByDia(day, this);
		((TextView) findViewById(R.id.ConsumoDia1)).setText("Consumo d�a "+(day+1));
		((TextView) findViewById(R.id.pcTotalElect)).setText((String.valueOf(e.precio).concat(" �")));
		((TextView)findViewById(R.id.cTotalElect)).setText((String.valueOf(e.consumo).concat(" W")));
	 }
	
	

}
