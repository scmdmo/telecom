package com.scm.telecon.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.scm.telecon.R;

public class EligeElectrodomesticoActivity extends FragmentActivity {
	
	public static final String RESULT="com.scm.telecon.EligeElectrodomesticoActivity.result";
	public static final String EXTRA_DATA="com.scm.telecon.EligeElectrodomesticoActivity.image_ids";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);			
		setContentView(R.layout.elige_electrodomesticos_main);
	}
}
