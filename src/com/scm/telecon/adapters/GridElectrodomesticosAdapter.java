package com.scm.telecon.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.scm.telecon.R;

public class GridElectrodomesticosAdapter extends BaseAdapter implements OnClickListener {
    private final Context context;
    //TODO: Estar�a bien a�adir alg�n electrodomestico m�s, y alg�n imagen general para un otros
    //Tambi�n es recomendable a�adirlos en carpteas(assets) externas y manejarlos fuera del hilo del UI 
    //con el uso de Asyntasks
    private int [] images_ids={
    						R.drawable.calefacion,
    						R.drawable.frigorifico,
    						R.drawable.horno,
    						R.drawable.lavadora,
    						R.drawable.luz,
    						R.drawable.luz2,
    						R.drawable.luz3,
    						R.drawable.agua,
    };
    
    /*Interfaz para lanzar las activities de datos sobre cada electrodom�stico*/
    private final Activityfeedback parentActivity;
    public interface Activityfeedback{
    	void activityResult(int image_ids);
    }
    
    
    //private final DBAElectrodomesticsServices dbaServices;  
    
    public GridElectrodomesticosAdapter(Context c,Activityfeedback parentActivity) {    	
    	context = c;
    	this.parentActivity = parentActivity;  
    	//dbaServices = new DBAElectrodomesticsServices(context);   
	}   
    
 	public int getCount() { 
 		//Log.d("DEBUG-getCount", "count="+dbaServices.getNumberElectrodomestics());
		return images_ids.length;
    }
    
    // no lo vamos a usar asi que lo dejamos vac�o
    public Object getItem(int position) {    	
        return null;
    }
    
    @Override
	public long getItemId(int position) {	
		return position;
	}
    
    public static class ViewHolder{
    	ImageView image;    	
    	int position;    	
    }
    
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {        
        final ViewHolder holder;
        if (convertView == null) {  // si no est� reciclada inicializamos algunos atributos        	
        	//inflate the layout       	
        	LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);        	
        	convertView = inflater.inflate(R.layout.row_grid, parent, false);
        	        	
        	//Creamos un ViewHolder        	
        	holder = new ViewHolder(); 
        	holder.image=(ImageView) convertView.findViewById(R.id.electrodomestic_image);        	
			holder.position=position;		
			
//			loadElectrodomesticView(position, convertView, holder);
			loadElectrodomesticView(position, convertView);
			
			//Store the holder with the views			
			convertView.setTag(holder);      
			
//			holder.image.setOnClickListener(new OnClickListener(){
//				@Override
//				public void onClick(View view) {					
//					ViewHolder holder = (ViewHolder) view.getTag();
//					Log.d("GridElectrodomesticosAdapter-holder", "holder="+(holder==null?"holder is null":"holder is not null"));
//					Log.d("GridElectrodomesticosAdapter-holder", "imagerid:"+holder.imageIDRes);
//					Log.d("GridElectrodomesticosAdapter-holder", "position"+holder.position);
//					parentActivity.activityResult(holder.position);
//				}});
			
			
			//holder.image.setOnClickListener(this);
			convertView.setOnClickListener(this);
      
        } else {
        	holder = (ViewHolder) convertView.getTag();        	
        	if(holder.position!=position){ 
            	loadElectrodomesticView(position, convertView);        	
            	holder.position=position;     
        	}
        }
        return convertView;
    }
    
    public void onClick(View view) {   	
    	
		ViewHolder holder = (ViewHolder) view.getTag();
//		Log.d("GridElectrodomesticosAdapter-holder", "holder="+(holder==null?"holder is null":"holder is not null"));
//		Log.d("GridElectrodomesticosAdapter-holder", "imagerid:"+holder.imageIDRes);
//		Log.d("GridElectrodomesticosAdapter-holder", "position"+holder.position);
		parentActivity.activityResult(images_ids[holder.position]);
//		parentActivity.activityResult(view.getId());
	}        
        
    private void loadElectrodomesticView(int position, View convertView){
    	//dbaServices.resetDB();
    	//nota: los _id (para las filas) del SQLlite empiezan en 1, las posiciones del gridview en 0. 
    	//DBElectrodomesticROW electrodomestic = dbaServices.getElectrodomestic((long) position+1);
    	ImageView image = (ImageView) convertView.findViewById(R.id.electrodomestic_image);    	
		image.setImageResource(this.images_ids[position]);	
	}
}


