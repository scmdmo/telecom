package com.scm.telecon.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scm.telecon.R;
import com.scm.telecon.activities.ElectrodomesticoActivity;
import com.scm.telecon.dbarows.DBElectrodomesticROW;
import com.scm.telecon.services.DBAElectrodomesticsServices;
import com.scm.telecon.services.Electrodomestic;
import com.scm.telecon.services.ReadXLS;

public class ListImageAdapter extends BaseAdapter {
    private final Context context;
    private final DBAElectrodomesticsServices dbaServices;

    int day = 0;
    /*Interfaz para lanzar las activities de datos sobre cada electrodom�stico*/
    private final ActivityLauncher launcher;
    public interface ActivityLauncher{
    	void launchActivity(Intent intent);
    }
    
    public ListImageAdapter(Context c,ActivityLauncher launcher) {    	
    	context = c;
    	dbaServices = new DBAElectrodomesticsServices(context);    	
    	//XXX: a�adimos datos por defecto, solo para debuging
    	dbaServices.resetDB();        
        
        this.launcher=launcher;
	}   
    
 	public int getCount() { 
 		//Log.d("DEBUG-getCount", "count="+dbaServices.getNumberElectrodomestics());
		return dbaServices.getNumberElectrodomestics();
    }
    
    // no lo vamos a usar asi que lo dejamos vac�o
    public Object getItem(int position) {    	
        return null;
    }
    
    @Override
	public long getItemId(int position) {	
		return position;
	}
    
    public static class ViewHolder{
    	ImageView image;
    	TextView  name;
    	TextView  consume;
    	int position;    	
    }
    
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {        
        final ViewHolder holder;
        if (convertView == null) {  // si no est� reciclada inicializamos algunos atributos        	
        	//inflate the layout       	
        	LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);        	
        	convertView = inflater.inflate(R.layout.row_list, parent, false);
        	        	
        	//Creamos un ViewHolder        	
        	holder = new ViewHolder(); 
        	holder.image=(ImageView) convertView.findViewById(R.id.electrodomestic_image);
			holder.name=(TextView) convertView.findViewById(R.id.electrodomestic_name);			
			holder.consume=(TextView) convertView.findViewById(R.id.electrodomestic_consume);
			holder.position=position;		
			
			loadElectrodomesticView(position, convertView);
			
			holder.image.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0) { 
					Intent intent= new Intent(context,ElectrodomesticoActivity.class);
					//TODO: a�adidos de la activity
//	                intent.putExtra(FullscreenActivity.POSITION,holder.position);
	                launcher.launchActivity(intent);
				}});
			
			//Store the holder with the views
			convertView.setTag(holder);            
        } else {
        	holder = (ViewHolder) convertView.getTag();        	
        	if(holder.position!=position) 
            	loadElectrodomesticView(position, convertView);        	
        	holder.position=position;        	
        }
//      Log.d("DEBUG-getView", "Entrando en load Electrodomestic:");
//    	Log.d("DEBUG-getView", "position: "+position);
//    	Log.d("DEBUG-getView", "convertView: "+convertView.getId());
//      loadElectrodomesticView(position, convertView);
        return convertView;
    }
    
    private void loadElectrodomesticView(int position, View convertView){    	
    	//nota: los _id (para las filas) del SQLlite empiezan en 1, las posiciones del listview en 0. 
    	DBElectrodomesticROW electrodomestic = dbaServices.getElectrodomestic((long) position+1);
    	ImageView image = (ImageView) convertView.findViewById(R.id.electrodomestic_image);
		TextView name = (TextView) convertView.findViewById(R.id.electrodomestic_name);			
		TextView consumo = (TextView) convertView.findViewById(R.id.electrodomestic_consume);
		
			
		image.setImageResource(electrodomestic.getImage_id().intValue());
		//EXCEDENT: codigo de debuguing 
//		if (electrodomestic!=null){
//			Log.d("DEBUG-loadElectrodomesticView", "_id="+electrodomestic.getImage_id().intValue());
//			Log.d("DEBUG-loadElectrodomesticView", "name= "+electrodomestic.getName());
//			Log.d("DEBUG-loadElectrodomesticView", "consumption= "+electrodomestic.getImage_id());
//			Log.d("DEBUG-loadElectrodomesticView", "image_id= "+electrodomestic.getImage_id());
//		} else
//			Log.d("DEBUG-loadElectrodomesticView", "electrodomestic=null");
		Log.d("MENSAJE", "Name: "+electrodomestic.getName().toString());
		name.setText(electrodomestic.getName().concat("     - Consumo Dia - "+(day+1)));
		if (electrodomestic.getName().toString().indexOf("Agua") != -1){
			Electrodomestic e = ReadXLS.getConsumoTotalByAguaByDia(day, this.context);
			consumo.setText(String.valueOf(e.consumo).concat(" L -> "+e.precio+" �"));
		}else{
			Electrodomestic e = ReadXLS.getDatosConsumoByNombreDia(electrodomestic.getName().toString(), day, this.context);
			consumo.setText(String.valueOf(e.consumo).concat(" W -> "+e.precio+" �"));
		}
	}
}


