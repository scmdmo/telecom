package com.scm.telecon.services;

import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

public class BarsChartFactoryService  {
	 
	private String chartTitle;	
	private String xTitle;
	private String yTitle;
	private List<String> xLabels;
	
	private final Context context;
    public BarsChartFactoryService(Context context,String chartTitle,String xTitle,String yTitle) {
    	this.context = context;
    	this.chartTitle = chartTitle;
    	this.xTitle = xTitle;
    	this.yTitle = yTitle;
    	this.xLabels = null;
	}
    
    public void setxLabels(List<String> xLabels) {
		this.xLabels = xLabels;
	}
    
    public Intent openChart(List<List<Float>> series, List<String> nameSeries){
    	
        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    	
        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle(this.chartTitle);
        multiRenderer.setShowAxes(true);
        multiRenderer.setAxesColor(150);
        multiRenderer.setBarSpacing(0.2d);
        multiRenderer.setXTitle(this.xTitle);
        multiRenderer.setYTitle(this.yTitle);
        multiRenderer.setZoomButtonsVisible(true);
        
        int r=130,g=130,b=230;
    	int j=0;
    	for(List<Float> serie:series){
    		XYSeries barSeries = new XYSeries(nameSeries.get(j));
    		    		
    		for(int i=0;i<serie.size();i++){
            	barSeries.add(i,serie.get(i).floatValue());            
            }
    		
    		XYSeriesRenderer barRenderer = new XYSeriesRenderer();
    		barRenderer.setFillPoints(true);
    		barRenderer.setLineWidth(2);
    		barRenderer.setDisplayBoundingPoints(true);
    		barRenderer.setDisplayChartValues(true);
    		barRenderer.setColor(Color.rgb((r+=90)%255, (g+205)%255,(b+105)%255));    		

    		// Adding  barRenderer to the multiRenderer    		
    		multiRenderer.addSeriesRenderer(barRenderer);
    		
    		// Adding  Series to the dataset
            dataset.addSeries(barSeries);
            j++;
    	}
    	
    	// Adding labels to axis X values
    	for(int i=0; i< xLabels.size();i++)            
            multiRenderer.addXTextLabel(i, xLabels.get(i));
 
        // Creating an intent to plot bar chart using dataset and multipleRenderer
        Intent intent = ChartFactory.getBarChartIntent(context, dataset, multiRenderer, Type.DEFAULT);
 
        // Start Activity
        return intent; 
    }	
}
