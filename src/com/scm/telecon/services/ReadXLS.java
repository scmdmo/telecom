package com.scm.telecon.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class ReadXLS {
	
	public static Electrodomestic getDatosConsumoByNombreDia(String name, int dia, Context context){
		AssetManager assetManager = context.getAssets();
		Electrodomestic e = null;
		try{
			String[]files = assetManager.list("data");
			String nameFile = files[dia];
			AssetManager asset = context.getAssets();
			InputStream in = asset.open("data/"+nameFile);
			Workbook workbook = Workbook.getWorkbook(in);
			Sheet sheet = workbook.getSheet(0);
			int numColumna = getNumColumnaByName(name, sheet);
			e = getDatosElectrodomestico(name, numColumna, sheet);			
			
		}catch(IOException e1){
			e1.printStackTrace();
			Log.d("LOG_ERROR", "IOException " + e1.getMessage());
		} catch (BiffException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		return e;
	}
	
	public static int getNumColumnaByName(String name, Sheet sheet){
		Cell [] celdas = sheet.getRow(0);
		for (int columna = 1; columna < celdas.length; columna++){
			if (celdas[columna].getContents().equalsIgnoreCase(name))
				return columna;
		}
		return 1;
	}
	
	public static int getCountDias(Context context){
		AssetManager assetManager = context.getAssets();
		String[] files = null;
		try {
			files = assetManager.list("data");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return files.length;
	}
	//get consumoTotal electrodomesticos by dia
	public static Electrodomestic getConsumoTotalByElectrodomesticosByDia(int dia, Context context){
		AssetManager assetManager = context.getAssets();
		Electrodomestic e = null;
		try{
			String[]files = assetManager.list("data");
			String nameFile = files[dia];
			AssetManager asset = context.getAssets();
			InputStream in = asset.open("data/"+nameFile);
			Workbook workbook = Workbook.getWorkbook(in);
			Sheet sheet = workbook.getSheet(0);
			int numColumna = getNumColumnaByName("Total", sheet);
			e = getDatosElectrodomestico("Total", numColumna, sheet);			
			
		}catch(IOException e1){
			e1.printStackTrace();
			Log.d("LOG_ERRORRRR", "IOException " + e1.getMessage());
		} catch (BiffException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			Log.d("LOG_ERRORRRR", "BiffException " + e2.getMessage());

		}
		return e;
	}
	
	public static Electrodomestic getConsumoTotalByAguaByDia(int dia, Context context){
		AssetManager assetManager = context.getAssets();
		Electrodomestic e = null;
		try{
			String[]files = assetManager.list("data");
			String nameFile = files[dia];
			AssetManager asset = context.getAssets();
			InputStream in = asset.open("data/"+nameFile);
			Workbook workbook = Workbook.getWorkbook(in);
			Sheet sheet = workbook.getSheet(0);
			int numColumna = getNumColumnaByName("Agua", sheet);
			e = getDatosElectrodomestico("Agua", numColumna, sheet);			
			
		}catch(IOException e1){
			e1.printStackTrace();
			Log.d("LOG_ERROR", "IOException " + e1.getMessage());
		} catch (BiffException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		return e;
	}
	
	public static List<Datos> read(Context context) {
		List <Datos> conjuntoDeDatos = null;
		AssetManager assetManager = context.getAssets();
		try{
			String[]files = assetManager.list("data");
			
			for (int i = 0; i< files.length; i++){
				conjuntoDeDatos.add(readXLS(files[0], context));
			}
			
			
		}catch(IOException e1){
			e1.printStackTrace();
			Log.d("LOG_ERROR", "IOException " + e1.getMessage());
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("LOG_ERROR", "BiffException " + e.getMessage());
		}
		return conjuntoDeDatos;
		
    }

	public static Datos readXLS(String nameFile, Context context) throws IOException, BiffException  {
		List<Electrodomestic> e = null;
		Electrodomestic elect = null;
		double consumoTotalElect = 0, consumoTotalAgua = 0;
		Datos datos = null;
		
		AssetManager assetManager = context.getAssets();
		InputStream in = assetManager.open("data/"+nameFile);
		Workbook workbook = Workbook.getWorkbook(in);
		Sheet sheet = workbook.getSheet(0);
			
		e.add(getDatosElectrodomestico("Nevera", 1, sheet));			
		e.add(getDatosElectrodomestico("Lavadora", 2, sheet));			
		e.add(getDatosElectrodomestico("Calefacción", 3, sheet));			
		e.add(getDatosElectrodomestico("Luz1", 4, sheet));			
		e.add(getDatosElectrodomestico("Luz2", 4, sheet));
		e.add(getDatosElectrodomestico("Luz3", 5, sheet));
		elect = getDatosElectrodomestico("Agua", 6, sheet);
		datos.consumos.consumoTotalAgua = elect.consumo;
		datos.consumos.consumoTotalAguaPrecio = elect.precio;
		//e.add(elect);
		
		elect = getDatosElectrodomestico("Total", 7, sheet);
		datos.consumos.consumoTotalElectrodomestico = elect.consumo;
		datos.consumos.consumoTotalElectPrecio = elect.precio;

		datos.electr = e;
		datos.dia = nameFile.substring(0, nameFile.indexOf(".")-1);
		return datos;
	}
	
	
	public static Electrodomestic getDatosElectrodomestico (String name, int columna, Sheet sheet){
		float valor = 0;
		String stringValor;
		
		Log.d("MENSAJE:","Entro en funcion");

		//Electrodomestic elect =  ReadXLS.new Electrodomestic();
		Electrodomestic elect = new Electrodomestic();
		
		Cell [] celdas = sheet.getColumn(columna); //fila
		for (int filas = 1; filas < celdas.length; filas++){
			stringValor = celdas[filas].getContents();
			if (!stringValor.equals("")){
				int agua = stringValor.indexOf("L");
				if (agua != -1){
					stringValor = stringValor.substring(0, agua);
				}
				valor += Float.valueOf(stringValor.replace(',', '.'));
			}
		}
		elect.nombre = name;
		elect.consumo = valor;
		if (name.indexOf("Agua")!=-1)
			elect.precio = (float) (valor*(0.0016));
		else
			elect.precio = (float) (valor*(0.11/1000));

		return elect;
	}

	public static Electrodomestic getDatosAgua(String name, int columna, Sheet sheet){
		float valor = 0;
		String stringValor;
		Electrodomestic elect = null;
		Cell [] celdas = sheet.getColumn(columna); //fila
		for (int filas = 1; filas < celdas.length; filas++){
			stringValor = celdas[filas].getContents();
			if (!stringValor.equalsIgnoreCase("")){
				stringValor = stringValor.substring(0, stringValor.length()-1);
				valor += Float.valueOf(stringValor);
			}
		}
		elect.nombre = name;
		elect.consumo = valor;
		elect.precio = (float) (valor*(0.0016));
		return elect;
	}
		
	
}


