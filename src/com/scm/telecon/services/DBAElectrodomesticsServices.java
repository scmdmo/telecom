package com.scm.telecon.services;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.scm.telecon.R;
import com.scm.telecon.adapters.DBAdapter;
import com.scm.telecon.dbarows.DBElectrodomesticROW;

public class DBAElectrodomesticsServices {
	
	private Context ctx;
	private DBAdapter db;	
	
	public DBAElectrodomesticsServices(Context ctx) {
		this.ctx = ctx;
		this.db = new DBAdapter(ctx);
	}
	
	public void resetDB(){		 
		 if (this.getNumberElectrodomestics()==0){
			 db.open();
		     db.insertEntry("Nevera", 300f, R.drawable.frigorifico);
		     db.insertEntry("Lavadora", 550f, R.drawable.lavadora);
		     db.insertEntry("Calefacci�n", 2000f, R.drawable.calefacion);
		     db.insertEntry("Luz 1", 11f, R.drawable.luz);
		     db.insertEntry("Luz 2", 20f, R.drawable.luz2);
		     db.insertEntry("Luz 3", 60f, R.drawable.luz3);
		     db.insertEntry("Horno", 2000f, R.drawable.horno);
		     db.insertEntry("Agua", 142f, R.drawable.agua);
		     db.close();
	     }
	     
	}
	
	public int getNumberElectrodomestics(){
		db.open();
		int res = db.getNumberRows();
		db.close();
		return res;
	}
	
	public void addElectrodomestic(String name, Float consupmtion, Integer imageID){
		 db.open();	     
		 db.insertEntry(name, consupmtion.floatValue(), imageID.intValue());
	     db.close();
	}     
     
	public List<DBElectrodomesticROW> getAllElectrodomestics(){		
		 List<DBElectrodomesticROW> list = new ArrayList<DBElectrodomesticROW>();
		 db.open();
	     Cursor c = db.getAllEntries();
	     if (c.moveToFirst())
	     {
	         do {
	        	 list.add(new DBElectrodomesticROW(c));	        	 
	             DisplayROW(c);
	         } while (c.moveToNext());
	     }
	     db.close();
	     return list;
	}
	
	public DBElectrodomesticROW getElectrodomestic(Long rowId){
		 db.open();
	     Cursor c = db.getEntry(rowId);
	     DBElectrodomesticROW res = null;
	     if (c.moveToFirst())        
	    	 res = new DBElectrodomesticROW(c);
	     else
	         Toast.makeText(this.ctx, "No Electrodomestic found", Toast.LENGTH_LONG).show();
	     db.close();    
		return res;
	} 
     
	public void uptadeElectrodomestic(String name, Float consupmtion, Integer imageID){
	//TODO: Under construction
     /*
     //---update example, update contact--- 
     db.open();
     if (db.updateContact(1, "Wei-Meng Lee", "weimenglee@gmail.com"))
         Toast.makeText(this, "Update successful.", Toast.LENGTH_LONG).show();
     else
         Toast.makeText(this, "Update failed.", Toast.LENGTH_LONG).show();
     db.close();
     */
	}
     
	public void deleteElectrodomestic(Long rowId){
	//TODO: Under construction	
     /*
     //---delete example, delete a contact---
     db.open();
     if (db.deleteContact(1))
         Toast.makeText(this, "Delete successful.", Toast.LENGTH_LONG).show();
     else
         Toast.makeText(this, "Delete failed.", Toast.LENGTH_LONG).show();
     db.close();
     */
	}     
   
	/**
	 * M�todo puramente para debuging
	 * @param c
	 */
	 public void DisplayROW(Cursor c){
	     Toast.makeText(this.ctx,
	             "id: " + c.getString(0) + "\n" +
	             "Name: " + c.getString(1) + "\n" +
	             "consuptiom:  " + String.valueOf(c.getFloat(2)) +
	             "image_id:  " + String.valueOf(c.getInt(3)),
	             Toast.LENGTH_LONG).show();
	}
}
