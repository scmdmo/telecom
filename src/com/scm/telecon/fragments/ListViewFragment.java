package com.scm.telecon.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.scm.telecon.R;
import com.scm.telecon.adapters.ListImageAdapter;

public class ListViewFragment extends Fragment implements ListImageAdapter.ActivityLauncher {
	
	private ListView list=null;
	private ListImageAdapter adapter=null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view=inflater.inflate(R.layout.list_fragment, container, false);			
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		super.onActivityCreated(savedInstanceState);		
		this.list=(ListView) getActivity().findViewById(R.id.listview);		
		adapter=new ListImageAdapter(getActivity().getBaseContext(),this);		
		list.setAdapter(adapter);	
	}
	
	@Override
	public void onStart() {	
		super.onStart();
		adapter.notifyDataSetChanged();		
	}

	@Override
	public void launchActivity(Intent intent) {
		startActivity(intent);
	}
}
