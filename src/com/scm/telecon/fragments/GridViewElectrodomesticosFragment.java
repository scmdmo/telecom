package com.scm.telecon.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.scm.telecon.R;
import com.scm.telecon.activities.EligeElectrodomesticoActivity;
import com.scm.telecon.adapters.GridElectrodomesticosAdapter;

public class GridViewElectrodomesticosFragment extends Fragment implements GridElectrodomesticosAdapter.Activityfeedback {
	
	private GridView grid=null;
	private GridElectrodomesticosAdapter adapter=null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view=inflater.inflate(R.layout.grid_fragment, container, false);			
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);		
		this.grid=(GridView) getActivity().findViewById(R.id.gridview);		
		adapter=new GridElectrodomesticosAdapter(getActivity().getBaseContext(),this);		
		grid.setAdapter(adapter);	
	}
	
	@Override
	public void onStart() {	
		super.onStart();
		adapter.notifyDataSetChanged();		
	}

	@Override
	public void activityResult(int image_ids) {
		Intent result = new Intent(EligeElectrodomesticoActivity.RESULT);
		result.putExtra(EligeElectrodomesticoActivity.EXTRA_DATA, image_ids);
		getActivity().setResult(Activity.RESULT_OK, result);		
		getActivity().finish();		
	}
}
