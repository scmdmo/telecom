package com.scm.telecon.dbarows;

import android.database.Cursor;

public class DBElectrodomesticROW {
	private Long id;
	private String name;
	private Float consupmtion;
	private Integer image_id;
	
	public DBElectrodomesticROW(Long id, String name, Float consupmtion, Integer image_id) {
		this.id = id;
		this.name = name;
		this.consupmtion = consupmtion;
		this.image_id = image_id;
	}	
	
	public DBElectrodomesticROW(Cursor c) {
		this.id = Long.valueOf(c.getLong(0));
		this.name = c.getString(1);
		this.consupmtion = Float.valueOf((c.getFloat(2)));
		this.image_id = Integer.valueOf((c.getInt(3)));
	}	
	
	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public Float getConsupmtion() {
		return consupmtion;
	}
	
	public Integer getImage_id() {
		return image_id;
	}
}
