package com.scm.telecon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.scm.telecon.R.id;
import com.scm.telecon.activities.ConsumoDeMisElectrodomesticosActivity;
import com.scm.telecon.activities.MisElectrodomesticosActivity;
import com.scm.telecon.services.ReadXLS;

public class MainActivity extends Activity {
	
	//FIXME: sobran por ahora
//	private TextView mis_electrodomesticos;
//	private Button electrodomesticos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
//		new DBAElectrodomesticsServices(this).resetDB();
//		mis_electrodomesticos = (TextView) findViewById(R.id.mis_electrodomesticos);
//		electrodomesticos = (Button) findViewById(R.id.electrodomesticos);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
    public void lanzarElectrodomesticos(View view) {
    	Intent mis_electrodomesticos = new Intent(this,MisElectrodomesticosActivity.class);
        startActivity(mis_electrodomesticos);    	
    }
    
    public void lanzarEstadisticasConsumos(View view) {
    	Intent consumo_de_mis_electrodomesticos = new Intent(this,ConsumoDeMisElectrodomesticosActivity.class);
        startActivity(consumo_de_mis_electrodomesticos);    	
    }

}
